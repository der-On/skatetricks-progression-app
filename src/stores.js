import { cloneDeep } from 'lodash';
import { writable } from 'svelte/store';
import initialState from './initialState.js';
import Trick from './models/Trick.js';
import TrickGroup from './models/TrickGroup.js';

export const search = writable(initialState.search);
export const tricks = writable(cloneDeep(initialState.tricks));
export const trickGroups = writable(cloneDeep(initialState.trickGroups));
export const settings = writable(cloneDeep(initialState.settings));
export const skatetricks = writable(cloneDeep(initialState.skatetricks));
export const version = writable(window.localStorage.version || initialState.version);
export const whatsNew = writable(initialState.whatsNew);
export const flashMessages = writable(cloneDeep(initialState.flashMessages));

if (window.localStorage.settings) {
  try {
    const _settings = JSON.parse(window.localStorage.settings);
    if (_settings) {
      settings.set(_settings);
    }
  } catch (err) {
    console.error(err);
  }
}

if (window.localStorage.tricks) {
  try {
    const _tricks = JSON.parse(window.localStorage.tricks);
    if (_tricks) {
      tricks.set(_tricks.map(Trick));
    }
  } catch (err) {
    console.error(err);
  }
}

if (window.localStorage.trickGroups) {
  try {
    const _trickGroups = JSON.parse(window.localStorage.trickGroups);
    if (_trickGroups) {
      trickGroups.set(_trickGroups.map(TrickGroup));
    }
  } catch (err) {
    console.error(err);
  }
}

settings.subscribe(value => {
  window.localStorage.settings = JSON.stringify(value);
});

tricks.subscribe(value => {
  window.localStorage.tricks = JSON.stringify(value.map((trick) => {
    return trick.asJson();
  }));
});

trickGroups.subscribe(value => {
  window.localStorage.trickGroups = JSON.stringify(value.map((trickGroup) => {
    return trickGroup.asJson();
  }));
});

version.subscribe(value => {
  window.localStorage.version = value;
});

async function loadSkatetricks() {
  const res = await fetch('./data/skatetricks.json');

  if (res.ok) {
    const json = await res.json();
    skatetricks.set(json);
  } else {
    const text = await res.text();
    throw new Error(text);
  }
}

loadSkatetricks();
