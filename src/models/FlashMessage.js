import { assign, findIndex } from 'lodash';
import { v4 } from 'uuid';
import { flashMessages } from '../stores.js';

let _flashMessages = [];

flashMessages.subscribe(value => {
  _flashMessages = value;
})

function FlashMessage(data = {}) {
  const flashMessage = assign({
    id: v4(),
    type: 'info',
    body: '',
    duration: 5000,
    createdAt: new Date(),
  }, data);

  flashMessage.destroy = function() {
    const index = findIndex(_flashMessages, ['id', flashMessage.id]);

    if (index !== -1) {
      _flashMessages.splice(index, 1);
      flashMessages.set(_flashMessages);
    }
  };

  if (flashMessage.duration) {
    setTimeout(flashMessage.destroy, flashMessage.duration);
  }

  return flashMessage;
}

export default FlashMessage;
