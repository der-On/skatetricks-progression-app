import {
  assign,
  repeat,
  reverse,
  sortBy,
} from 'lodash';
import { v4 } from 'uuid';
import { tricks } from '../stores.js';

let _tricks = [];
let hasSubscribedTricks = false;

function TrickGroup(data = {}) {
  const trickGroup = assign({
    id: v4(),
    name: null,
    isExpanded: true,
    sortOrder: 0,
  }, data);

  if (!hasSubscribedTricks) {
    tricks.subscribe(value => {
      _tricks = value;
    });
    hasSubscribedTricks = true;
  }

  function isTrickInGroup(trick) {
    return trick.groupId === trickGroup.id;
  }

  trickGroup.getTricks = function () {
    return reverse(
      sortBy(_tricks, ['sortOrder'])
      .filter(isTrickInGroup)
    );
  };

  trickGroup.getTricksProgress = function () {
    const tricks = trickGroup.getTricks();

    return tricks.reduce((total, trick) => {
      return trick.progress + total;
    }, 0) / tricks.length;
  };

  trickGroup.asText = function () {
    return `
${trickGroup.name}
${repeat('=', trickGroup.name.length - 1)}

Progress: ${Math.round(trickGroup.getTricksProgress() * 100)}%

Tricks
======

${trickGroup.getTricks().map(trick => { return trick.asText(); }).join('\n')}
`.trim();
  };

  trickGroup.asHtml = function () {
    return `
<h2>${trickGroup.name}</h2>
<p>
  <strong>Progress:</strong> ${Math.round(trickGroup.getTricksProgress() * 100)}%
</p>
<h3>Tricks</h3>
<ul>
  ${trickGroup.getTricks().map(trick => { return `<li>${trick.asHtml()}</li>`; }).join('\n')}
</ul>
`.trim();
  };

  trickGroup.asJson = function () {
    return {
      id: trickGroup.id,
      name: trickGroup.name,
      isExpanded: trickGroup.isExpanded,
      sortOrder: trickGroup.sortOrder,
    };
  };

  return trickGroup;
}

export default TrickGroup;
