export default {
  h2: 'text-2xl font-semibold text-gray-900',
  h3: 'text-lg font-semibold text-gray-900',
  label: 'text-sm font-medium text-gray-700',
  input: 'shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md',
  buttonPrimary: 'inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500',
  buttonSecondary: 'inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-base font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500',
  buttonGrayIcon: 'text-gray-900 opacity-25 hover:opacity-50 focus:opacity-50 p-2 cursor-pointer',
  card: 'bg-white border border-gray-200 shadow-md rounded-md',
};
