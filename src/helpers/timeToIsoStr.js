import { padStart } from 'lodash';

function timeToIsoStr(date) {
  return [
    padStart(date.getHours() + '', 2, '0'),
    padStart(date.getMinutes() + '', 2, '0'),
    padStart(date.getSeconds() + '', 2, '0')
  ].join(':');
}

export default timeToIsoStr;
