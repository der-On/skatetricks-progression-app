import { get } from 'lodash';
import pkg from '../package.json';
import {
  version,
  settings,
  whatsNew
} from './stores.js';
import updates from './updates.js';

let _version;
let _tricks;
let _whatsNew;
version.subscribe(value => {
  _version = value;
});
whatsNew.subscribe(value => {
  _whatsNew = value;
});

function addWhatsNew(v, msg) {
  whatsNew.set(`
    <h3>Version: ${v}</h3>
    ${msg}
  ` + _whatsNew);
}

function setVersion(v) {
  version.set(v);
}

function upgrade(vFrom, vTo, silent = false) {
  console.log(`updating from ${vFrom} to ${vTo}`);

  updates.forEach(update => {
    if (update.v > vFrom && update.v <= vTo) {
      if (!silent && update.changelog) {
        addWhatsNew(update.v, update.changelog);
      }
      if (update.migrations) {
        update.migrations.forEach(migration => {
          migration();
        });
      }
    }
  });

  setVersion(vTo);
}

function migrate(options = {}) {
  const silent = !!options.silent;
  if (_version != pkg.version) {
    upgrade(_version, pkg.version, silent);
  }
}

export default migrate;
