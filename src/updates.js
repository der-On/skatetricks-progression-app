import { sortBy, isUndefined } from 'lodash';
import {
  tricks,
  settings,
} from './stores.js';

let _tricks;
tricks.subscribe(value => {
  _tricks = value || [];
});

export default [
  {
    v: '0.0.1',
    changelog: '<p>Welcome to the skateboarding progression app.</p>',
  },
  {
    v: '0.0.2',
    changelog: '<p>You can now drag & drop your tricks to resort them.</p>',
    migrations: [() => {
      const updatedTricks = sortBy(_tricks, 'sortOrder')
      .map((trick, index) => {
        if (isUndefined(trick.sortOrder)) {
          trick.sortOrder = index;
        }
        return trick;
      });

      tricks.set(updatedTricks);
    }]
  },
  {
    v: '0.0.3',
    changelog: `<p>You can now backup and restore all your data. Find it in Settings > Backup.</p>
    <p>You can export all your tricks in different formats to share them with your friends! Find it in Settings > Export my tricks.</p>`,
  },
  {
    v: '0.0.4',
    changelog: `<p>Various bugfixes.</p>
    <p>Add's a compact tricks view.<br/>To toggle it, tap the icon on the right to the total trick progress.</p>`,
  },
  {
    v: '0.0.5',
    changelog: `<p>You can now group tricks.</p>
    <p>Various bugfixes and improvements</p>`,
  },
  {
    v: '0.0.6',
    changelog: `<p>The app has moved to a new home.</p>
    <p>Please create a <a href="#/settings" target="settings">backup</a> of your data and use one of the following URLs to access the app:</p>
    <ul>
      <li><a href="https://skatetricks-progression.anzui.dev" target="anzui-app">https://skatetricks-progression.anzui.dev</a> (might be gone eventually one day)</li>
      <li><a href="https://der_on.codeberg.page/skatetricks-progression-app/@pages" target="codeberg-app">https://der_on.codeberg.page/skatetricks-progression-app/@pages</a> (should stay as long as <a href="https://codeberg.org">codeberg</a> exists)</li>
    </ul>
    <p>The app over at <a href="https://der-on.gitlab.io/skatetricks-progression-app" target="gitlab-app">https://der-on.gitlab.io/skatetricks-progression-app</a> will not get any updates and will soon be removed from that place. So please switch to one of the new URLs above.</p>`
  },
];
