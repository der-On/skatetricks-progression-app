export default {
  search: '',
  tricks: [],
  trickGroups: [],
  version: '0.0.0',
  settings: {
    darkMode: false,
    tricksAreCompact: false,
  },
  flashMessages: [],
  whatsNew: '',
  skatetricks: {
    tricks: [],
    stances: [],
  },
};
